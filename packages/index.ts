/*
 * @Author: lizhijie429
 * @Date: 2021-06-22 08:52:55
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-06-22 09:04:33
 * @Description:
 */
/* @remove-on-es-build-end */
import { App } from "vue";
const components = [];
const install = function (app: App) {
  console.log("./");
  components.forEach((component) => {
    app.use(component);
  });
};
export { install };
