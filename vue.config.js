/*
 * @Author: lizhijie429
 * @Date: 2021-06-22 08:45:44
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-06-22 08:52:19
 * @Description:
 */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, "..", dir);
}
module.exports = {
  // 为packages目录添加babel-loader处理
  chainWebpack: (config) => {
    config.module
      .rule("js")
      .include.add(resolve("packages"))
      .end()
      .use("babel")
      .loader("babel-loader")
      .tap((options) => {
        return options;
      });
  },
};
